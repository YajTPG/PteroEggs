
# CHANGELOG

⟶ Added NodeJS 18 \
⟶ Removed older and non-LTS NodeJS versions
## 📌 Messages

⟶ Deprecating the `debian` branch, multi-version eggs will be actively supported from now onward.
